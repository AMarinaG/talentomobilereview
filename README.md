# TalentoMobile Review

Prueba de código consistente en 2 ventanas:
* un buscador
* un mapa de google donde muestra la localizaciones devueltas por un servicio


## Tecnología y librerías
* Android api 16+ (4.1+) (Jelly Bean)
* MVP + clean + arquitectura de capas (repositorios y servicios)
* Dagger2 para la inyección de dependencias
* Retrofit para las peticiones al servidor
* Gson para el parseo
* ORMLite para la base de datos
* EventBus para la conexión entre capas
* Stetho para debug de peticiones y db.
* Fabric para analítica, y eventos (Answers)




### TODO LIST
* Añadir test. (Falta de tiempo)
* Control de errores. (Falta de tiempo)
* Eliminar Logs por consola(Cambiar level de log -> requisito de la prueba)
* Añadir imagen en la ventana del buscador.
* Todo lo que se pueda ocurrir.