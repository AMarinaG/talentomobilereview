package com.amarinag.talentomobilereview.db;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public interface DatabaseService {
  void saveGeoname(GeonameEntity geonameEntity);

  void saveGeoname(List<GeonameEntity> geonameEntities);

  List<GeonameEntity> getGeonames();
}
