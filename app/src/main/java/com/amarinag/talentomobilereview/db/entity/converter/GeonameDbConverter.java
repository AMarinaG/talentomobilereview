package com.amarinag.talentomobilereview.db.entity.converter;

import com.amarinag.talentomobilereview.db.entity.GeonameDb;
import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public abstract class GeonameDbConverter {
  public static List<GeonameEntity> wrap(Collection<GeonameDb> collectionDb) {
    List<GeonameEntity> entities = new ArrayList<>(collectionDb.size());
    for (GeonameDb geonameDb : collectionDb) {
      entities.add(wrap(geonameDb));
    }
    return entities;
  }

  public static GeonameEntity wrap(GeonameDb db) {
    if (db == null) {
      return new GeonameEntity();
    }
    GeonameEntity entity = new GeonameEntity();
    entity.setTimezone(TimezoneDbConverter.wrap(db.getTimezone()));
    entity.setBbox(BboxDbConverter.wrap(db.getBbox()));
    entity.setAsciiName(db.getAsciiName());
    entity.setCountryId(db.getCountryId());
    entity.setFcl(db.getFcl());
    entity.setScore(db.getScore());
    entity.setAdminId2(db.getAdminId2());
    entity.setAdminId3(db.getAdminId3());
    entity.setCountryCode(db.getCountryCode());
    entity.setAdminId1(db.getAdminId1());
    entity.setLat(db.getLat());
    entity.setFcode(db.getFcode());
    entity.setContinentCode(db.getContinentCode());
    entity.setAdminCode2(db.getAdminCode2());
    entity.setAdminCode3(db.getAdminCode3());
    entity.setAdminCode1(db.getAdminCode1());
    entity.setLng(db.getLng());
    entity.setGeonameId(db.getGeonameId());
    entity.setToponymName(db.getToponymName());
    entity.setPopulation(db.getPopulation());
    entity.setAdminName5(db.getAdminName5());
    entity.setAdminName4(db.getAdminName4());
    entity.setAdminName3(db.getAdminName3());
    entity.setAdminName2(db.getAdminName2());
    entity.setName(db.getName());
    entity.setFclName(db.getFclName());
    entity.setCountryName(db.getCountryName());
    entity.setFcodeName(db.getFcodeName());
    entity.setAdminName1(db.getAdminName1());
    entity.setAdminTypeName(db.getAdminTypeName());
    entity.setElevation(db.getElevation());
    entity.setCc2(db.getCc2());
    return entity;
  }

  public static GeonameDb unwrap(GeonameEntity entity) {
    if (entity == null) {
      return new GeonameDb();
    }
    GeonameDb db = new GeonameDb();
    db.setTimezone(TimezoneDbConverter.unwrap(entity.getTimezone()));
    db.setBbox(BboxDbConverter.unwrap(entity.getBbox()));
    db.setAsciiName(entity.getAsciiName());
    db.setCountryId(entity.getCountryId());
    db.setFcl(entity.getFcl());
    db.setScore(entity.getScore());
    db.setAdminId2(entity.getAdminId2());
    db.setAdminId3(entity.getAdminId3());
    db.setCountryCode(entity.getCountryCode());
    db.setAdminId1(entity.getAdminId1());
    db.setLat(entity.getLat());
    db.setFcode(entity.getFcode());
    db.setContinentCode(entity.getContinentCode());
    db.setAdminCode2(entity.getAdminCode2());
    db.setAdminCode3(entity.getAdminCode3());
    db.setAdminCode1(entity.getAdminCode1());
    db.setLng(entity.getLng());
    db.setGeonameId(entity.getGeonameId());
    db.setToponymName(entity.getToponymName());
    db.setPopulation(entity.getPopulation());
    db.setAdminName5(entity.getAdminName5());
    db.setAdminName4(entity.getAdminName4());
    db.setAdminName3(entity.getAdminName3());
    db.setAdminName2(entity.getAdminName2());
    db.setName(entity.getName());
    db.setFclName(entity.getFclName());
    db.setCountryName(entity.getCountryName());
    db.setFcodeName(entity.getFcodeName());
    db.setAdminName1(entity.getAdminName1());
    db.setAdminTypeName(entity.getAdminTypeName());
    db.setElevation(entity.getElevation());
    db.setCc2(entity.getCc2());
    return db;
  }
}
