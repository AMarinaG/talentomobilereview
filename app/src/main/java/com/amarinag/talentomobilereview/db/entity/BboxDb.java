package com.amarinag.talentomobilereview.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
@DatabaseTable(tableName = "bbox")
public class BboxDb {
  @DatabaseField(generatedId = true)
  private int id;
  @DatabaseField
  private float east;
  @DatabaseField
  private float south;
  @DatabaseField
  private float north;
  @DatabaseField
  private float west;
  @DatabaseField
  private int accuracyLevel;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public float getEast() {
    return east;
  }

  public void setEast(float east) {
    this.east = east;
  }

  public float getSouth() {
    return south;
  }

  public void setSouth(float south) {
    this.south = south;
  }

  public float getNorth() {
    return north;
  }

  public void setNorth(float north) {
    this.north = north;
  }

  public float getWest() {
    return west;
  }

  public void setWest(float west) {
    this.west = west;
  }

  public int getAccuracyLevel() {
    return accuracyLevel;
  }

  public void setAccuracyLevel(int accuracyLevel) {
    this.accuracyLevel = accuracyLevel;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("BboxDb{");
    sb.append("id=").append(id);
    sb.append(", east=").append(east);
    sb.append(", south=").append(south);
    sb.append(", north=").append(north);
    sb.append(", west=").append(west);
    sb.append(", accuracyLevel=").append(accuracyLevel);
    sb.append('}');
    return sb.toString();
  }
}
