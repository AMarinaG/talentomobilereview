package com.amarinag.talentomobilereview.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
@DatabaseTable(tableName = "geoname")
public class GeonameDb {
  @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
  private TimezoneDb timezone;
  @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
  private BboxDb bbox;
  @DatabaseField
  private String asciiName;
  @DatabaseField
  private String countryId;
  @DatabaseField
  private String fcl;
  @DatabaseField
  private float score;
  @DatabaseField
  private String adminId2;
  @DatabaseField
  private String adminId3;
  @DatabaseField
  private String countryCode;
  @DatabaseField
  private String adminId1;
  @DatabaseField
  private String lat;
  @DatabaseField
  private String fcode;
  @DatabaseField
  private String continentCode;
  @DatabaseField
  private String adminCode2;
  @DatabaseField
  private String adminCode3;
  @DatabaseField
  private String adminCode1;
  @DatabaseField
  private String lng;
  @DatabaseField(id = true)
  private int geonameId;
  @DatabaseField
  private String toponymName;
  @DatabaseField
  private int population;
  @DatabaseField
  private String adminName5;
  @DatabaseField
  private String adminName4;
  @DatabaseField
  private String adminName3;
  @DatabaseField
  private String adminName2;
  @DatabaseField
  private String name;
  @DatabaseField
  private String fclName;
  @DatabaseField
  private String countryName;
  @DatabaseField
  private String fcodeName;
  @DatabaseField
  private String adminName1;
  @DatabaseField
  private String adminTypeName;
  @DatabaseField
  private int elevation;
  @DatabaseField
  private String cc2;

  public TimezoneDb getTimezone() {
    return timezone;
  }

  public void setTimezone(TimezoneDb timezone) {
    this.timezone = timezone;
  }

  public BboxDb getBbox() {
    return bbox;
  }

  public void setBbox(BboxDb bbox) {
    this.bbox = bbox;
  }

  public String getAsciiName() {
    return asciiName;
  }

  public void setAsciiName(String asciiName) {
    this.asciiName = asciiName;
  }

  public String getCountryId() {
    return countryId;
  }

  public void setCountryId(String countryId) {
    this.countryId = countryId;
  }

  public String getFcl() {
    return fcl;
  }

  public void setFcl(String fcl) {
    this.fcl = fcl;
  }

  public float getScore() {
    return score;
  }

  public void setScore(float score) {
    this.score = score;
  }

  public String getAdminId2() {
    return adminId2;
  }

  public void setAdminId2(String adminId2) {
    this.adminId2 = adminId2;
  }

  public String getAdminId3() {
    return adminId3;
  }

  public void setAdminId3(String adminId3) {
    this.adminId3 = adminId3;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getAdminId1() {
    return adminId1;
  }

  public void setAdminId1(String adminId1) {
    this.adminId1 = adminId1;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getFcode() {
    return fcode;
  }

  public void setFcode(String fcode) {
    this.fcode = fcode;
  }

  public String getContinentCode() {
    return continentCode;
  }

  public void setContinentCode(String continentCode) {
    this.continentCode = continentCode;
  }

  public String getAdminCode2() {
    return adminCode2;
  }

  public void setAdminCode2(String adminCode2) {
    this.adminCode2 = adminCode2;
  }

  public String getAdminCode3() {
    return adminCode3;
  }

  public void setAdminCode3(String adminCode3) {
    this.adminCode3 = adminCode3;
  }

  public String getAdminCode1() {
    return adminCode1;
  }

  public void setAdminCode1(String adminCode1) {
    this.adminCode1 = adminCode1;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public int getGeonameId() {
    return geonameId;
  }

  public void setGeonameId(int geonameId) {
    this.geonameId = geonameId;
  }

  public String getToponymName() {
    return toponymName;
  }

  public void setToponymName(String toponymName) {
    this.toponymName = toponymName;
  }

  public int getPopulation() {
    return population;
  }

  public void setPopulation(int population) {
    this.population = population;
  }

  public String getAdminName5() {
    return adminName5;
  }

  public void setAdminName5(String adminName5) {
    this.adminName5 = adminName5;
  }

  public String getAdminName4() {
    return adminName4;
  }

  public void setAdminName4(String adminName4) {
    this.adminName4 = adminName4;
  }

  public String getAdminName3() {
    return adminName3;
  }

  public void setAdminName3(String adminName3) {
    this.adminName3 = adminName3;
  }

  public String getAdminName2() {
    return adminName2;
  }

  public void setAdminName2(String adminName2) {
    this.adminName2 = adminName2;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFclName() {
    return fclName;
  }

  public void setFclName(String fclName) {
    this.fclName = fclName;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getFcodeName() {
    return fcodeName;
  }

  public void setFcodeName(String fcodeName) {
    this.fcodeName = fcodeName;
  }

  public String getAdminName1() {
    return adminName1;
  }

  public void setAdminName1(String adminName1) {
    this.adminName1 = adminName1;
  }

  public String getAdminTypeName() {
    return adminTypeName;
  }

  public void setAdminTypeName(String adminTypeName) {
    this.adminTypeName = adminTypeName;
  }

  public int getElevation() {
    return elevation;
  }

  public void setElevation(int elevation) {
    this.elevation = elevation;
  }

  public String getCc2() {
    return cc2;
  }

  public void setCc2(String cc2) {
    this.cc2 = cc2;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("GeonameDb{");
    sb.append("timezone=").append(timezone);
    sb.append(", bbox=").append(bbox);
    sb.append(", asciiName='").append(asciiName).append('\'');
    sb.append(", countryId='").append(countryId).append('\'');
    sb.append(", fcl='").append(fcl).append('\'');
    sb.append(", score=").append(score);
    sb.append(", adminId2='").append(adminId2).append('\'');
    sb.append(", adminId3='").append(adminId3).append('\'');
    sb.append(", countryCode='").append(countryCode).append('\'');
    sb.append(", adminId1='").append(adminId1).append('\'');
    sb.append(", lat='").append(lat).append('\'');
    sb.append(", fcode='").append(fcode).append('\'');
    sb.append(", continentCode='").append(continentCode).append('\'');
    sb.append(", adminCode2='").append(adminCode2).append('\'');
    sb.append(", adminCode3='").append(adminCode3).append('\'');
    sb.append(", adminCode1='").append(adminCode1).append('\'');
    sb.append(", lng='").append(lng).append('\'');
    sb.append(", geonameId=").append(geonameId);
    sb.append(", toponymName='").append(toponymName).append('\'');
    sb.append(", population=").append(population);
    sb.append(", adminName5='").append(adminName5).append('\'');
    sb.append(", adminName4='").append(adminName4).append('\'');
    sb.append(", adminName3='").append(adminName3).append('\'');
    sb.append(", adminName2='").append(adminName2).append('\'');
    sb.append(", name='").append(name).append('\'');
    sb.append(", fclName='").append(fclName).append('\'');
    sb.append(", countryName='").append(countryName).append('\'');
    sb.append(", fcodeName='").append(fcodeName).append('\'');
    sb.append(", adminName1='").append(adminName1).append('\'');
    sb.append(", adminTypeName='").append(adminTypeName).append('\'');
    sb.append(", elevation=").append(elevation);
    sb.append(", cc2='").append(cc2).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
