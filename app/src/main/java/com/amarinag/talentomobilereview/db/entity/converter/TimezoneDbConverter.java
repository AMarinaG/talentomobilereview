package com.amarinag.talentomobilereview.db.entity.converter;

import com.amarinag.talentomobilereview.db.entity.TimezoneDb;
import com.amarinag.talentomobilereview.domain.entity.search.TimezoneEntity;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public class TimezoneDbConverter {
  public static TimezoneEntity wrap(TimezoneDb db) {
    if (db == null) {
      return new TimezoneEntity();
    }
    TimezoneEntity entity = new TimezoneEntity();
    entity.setGmtOffset(db.getGmtOffset());
    entity.setTimeZoneId(db.getTimeZoneId());
    entity.setDstOffset(db.getDstOffset());
    return entity;
  }

  public static TimezoneDb unwrap(TimezoneEntity entity) {
    if (entity == null) {
      return new TimezoneDb();
    }
    TimezoneDb db = new TimezoneDb();
    db.setGmtOffset(entity.getGmtOffset());
    db.setTimeZoneId(entity.getTimeZoneId());
    db.setDstOffset(entity.getDstOffset());
    return db;
  }
}
