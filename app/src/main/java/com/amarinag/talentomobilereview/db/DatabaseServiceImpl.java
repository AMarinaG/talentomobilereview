package com.amarinag.talentomobilereview.db;

import com.amarinag.talentomobilereview.db.entity.GeonameDb;
import com.amarinag.talentomobilereview.db.entity.converter.GeonameDbConverter;
import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.j256.ormlite.dao.Dao;
import timber.log.Timber;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public class DatabaseServiceImpl implements DatabaseService {
  private Dao<GeonameDb, Integer> geonameDao;

  public DatabaseServiceImpl(Dao<GeonameDb, Integer> geonameDao) {
    this.geonameDao = geonameDao;
  }

  @Override
  public void saveGeoname(GeonameEntity geonameEntity) {
    try {
      geonameDao.createIfNotExists(GeonameDbConverter.unwrap(geonameEntity));
    } catch (SQLException e) {
      Timber.e(e, "No se ha podido crear %s", geonameEntity);
    }
  }

  @Override
  public void saveGeoname(List<GeonameEntity> geonameEntities) {
    for (GeonameEntity geonameEntity : geonameEntities) {
      saveGeoname(geonameEntity);
    }
  }

  @Override
  public List<GeonameEntity> getGeonames() {
    try {
      List<GeonameDb> rows = geonameDao.queryForAll();
      return GeonameDbConverter.wrap(rows);
    } catch (SQLException sqle) {
      Timber.e(sqle, "No se ha podido obtener el listado de geonames: %s", sqle.getMessage());
      return Collections.EMPTY_LIST;
    }
  }
}
