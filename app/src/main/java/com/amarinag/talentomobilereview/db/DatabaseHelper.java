package com.amarinag.talentomobilereview.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.amarinag.talentomobilereview.BuildConfig;
import com.amarinag.talentomobilereview.db.entity.BboxDb;
import com.amarinag.talentomobilereview.db.entity.GeonameDb;
import com.amarinag.talentomobilereview.db.entity.TimezoneDb;
import com.amarinag.talentomobilereview.di.ForApplication;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import timber.log.Timber;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
@Singleton
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
  private Dao<GeonameDb, Integer> geonameDao;

  @Inject
  public DatabaseHelper(@ForApplication Context context) {
    super(context, BuildConfig.DATABASE_NAME, null, BuildConfig.DATABASE_VERSION);
  }

  public Dao<GeonameDb, Integer> getGeonameDao() {
    if (geonameDao == null) {
      try {
        geonameDao = getDao(GeonameDb.class);
      } catch (SQLException sqle) {
        Timber.e(sqle, "Error al obtener GeonameDao: %s", sqle.getMessage());
      }
    }
    return geonameDao;
  }

  @Override
  public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
    try {
      TableUtils.createTable(connectionSource, GeonameDb.class);
      TableUtils.createTable(connectionSource, BboxDb.class);
      TableUtils.createTable(connectionSource, TimezoneDb.class);
    } catch (SQLException sqle) {
      Timber.e(sqle, "Error al crear la db: %s", sqle.getMessage());
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                        int oldVersion, int newVersion) {
    try {
      TableUtils.dropTable(connectionSource, GeonameDb.class, true);
      TableUtils.dropTable(connectionSource, BboxDb.class, true);
      TableUtils.dropTable(connectionSource, TimezoneDb.class, true);
      onCreate(database, connectionSource);
    } catch (SQLException sqle) {
      Timber.e(sqle, "Error al updatear la bd: %s", sqle.getMessage());

    }
    onCreate(database, connectionSource);
  }
}
