package com.amarinag.talentomobilereview.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
@DatabaseTable(tableName = "timezone")
public class TimezoneDb {
  @DatabaseField
  private double gmtOffset;
  @DatabaseField(id = true)
  private String timeZoneId;
  @DatabaseField
  private double dstOffset;

  public double getGmtOffset() {
    return gmtOffset;
  }

  public void setGmtOffset(double gmtOffset) {
    this.gmtOffset = gmtOffset;
  }

  public String getTimeZoneId() {
    return timeZoneId;
  }

  public void setTimeZoneId(String timeZoneId) {
    this.timeZoneId = timeZoneId;
  }

  public double getDstOffset() {
    return dstOffset;
  }

  public void setDstOffset(double dstOffset) {
    this.dstOffset = dstOffset;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("TimezoneDb{");
    sb.append("gmtOffset=").append(gmtOffset);
    sb.append(", timeZoneId='").append(timeZoneId).append('\'');
    sb.append(", dstOffset=").append(dstOffset);
    sb.append('}');
    return sb.toString();
  }
}
