package com.amarinag.talentomobilereview.db.entity.converter;

import com.amarinag.talentomobilereview.db.entity.BboxDb;
import com.amarinag.talentomobilereview.domain.entity.search.BboxEntity;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public abstract class BboxDbConverter {
  public static BboxEntity wrap(BboxDb db) {
    if (db == null) {
      return new BboxEntity();
    }
    BboxEntity entity = new BboxEntity();
    entity.setEast(db.getEast());
    entity.setSouth(db.getSouth());
    entity.setNorth(db.getNorth());
    entity.setWest(db.getWest());
    entity.setAccuracyLevel(db.getAccuracyLevel());
    return entity;
  }

  public static BboxDb unwrap(BboxEntity entity) {
    if (entity == null) {
      return new BboxDb();
    }
    BboxDb db = new BboxDb();
    db.setEast(entity.getEast());
    db.setSouth(entity.getSouth());
    db.setNorth(entity.getNorth());
    db.setWest(entity.getWest());
    db.setAccuracyLevel(entity.getAccuracyLevel());
    return db;
  }
}
