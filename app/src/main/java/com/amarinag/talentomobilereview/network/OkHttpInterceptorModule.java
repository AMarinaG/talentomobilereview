package com.amarinag.talentomobilereview.network;

import android.support.annotation.NonNull;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@Module
public class OkHttpInterceptorModule {

  @Provides
  @Singleton
  @NonNull
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.d(message));
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
    return loggingInterceptor;
  }

  @Provides
  @OkHttpInterceptors
  @Singleton
  @NonNull
  public List<Interceptor> provideOkHttpInterceptors(@NonNull HttpLoggingInterceptor httpLoggingInterceptor) {
    return Collections.singletonList(httpLoggingInterceptor);
  }

  @Provides
  @OkHttpNetworkInterceptors
  @Singleton
  @NonNull
  public List<Interceptor> provideOkHttpNetworkInterceptors() {
    return Collections.singletonList(new StethoInterceptor());
  }
}
