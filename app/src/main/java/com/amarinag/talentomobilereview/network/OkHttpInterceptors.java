package com.amarinag.talentomobilereview.network;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Qualifier;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@Documented
@Qualifier
@Retention(RUNTIME)
public @interface OkHttpInterceptors {
}
