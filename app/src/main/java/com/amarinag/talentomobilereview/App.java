package com.amarinag.talentomobilereview;

import android.app.Application;
import android.content.Context;
import com.amarinag.talentomobilereview.di.component.AppComponent;
import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   24/5/17
 */
public class App extends Application {
  private AppComponent appComponent;
  private RefWatcher refWatcher;

  public static App get(Context context) {
    return (App) context.getApplicationContext();
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics());

    refWatcher = LeakCanary.install(this);

    appComponent = createComponent();
    Timber.plant(new Timber.DebugTree());
    Stetho.initializeWithDefaults(this);

    Timber.d(BuildConfig.EASTER_EGG);
  }

  public RefWatcher getRefWatcher() {
    if (refWatcher == null) {
      refWatcher = LeakCanary.install(this);
    }
    return refWatcher;
  }

  private AppComponent createComponent() {
    appComponent = AppComponent.Initializer.init(this);
    appComponent.inject(this);
    return appComponent;
  }

  public void recreateComponent() {
    appComponent = AppComponent.Initializer.init(this);
    appComponent.inject(this);
  }

  public AppComponent getComponent() {
    return appComponent;
  }

  public void setAppComponent(AppComponent appComponent) {
    this.appComponent = appComponent;
  }
}
