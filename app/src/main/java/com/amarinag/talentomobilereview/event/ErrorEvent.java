package com.amarinag.talentomobilereview.event;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   7/6/17
 */
public class ErrorEvent {
  private ErrorType type;
  private String message;

  private ErrorEvent(String message) {
    this(ErrorType.MESSAGE, message);
  }

  private ErrorEvent(ErrorType type) {
    this(type, null);
  }

  private ErrorEvent(ErrorType type, String message) {
    this.type = type;
    this.message = message;
  }

  public static ErrorEvent createErrorEvent(String message) {
    return new ErrorEvent(message);
  }

  public static ErrorEvent createServerDownError() {
    return new ErrorEvent(ErrorType.SERVER_DOWN);
  }

  public ErrorType getType() {
    return type;
  }

  public String getMessage() {
    return message;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("ErrorEvent{");
    sb.append("type=").append(type);
    sb.append(", message='").append(message).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
