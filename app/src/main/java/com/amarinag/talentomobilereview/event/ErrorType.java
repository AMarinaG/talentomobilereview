package com.amarinag.talentomobilereview.event;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   7/6/17
 */
public enum ErrorType {
  UNKNOW,
  SERVER_DOWN,
  MESSAGE
}
