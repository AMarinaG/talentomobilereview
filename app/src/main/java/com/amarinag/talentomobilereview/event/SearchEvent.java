package com.amarinag.talentomobilereview.event;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.domain.entity.search.SearchResponse;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   29/5/17
 */
public class SearchEvent {
  private SearchResponse searchResponse;

  public SearchEvent(SearchResponse searchResponse) {
    this.searchResponse = searchResponse;
  }

  public SearchResponse getSearchResponse() {
    return searchResponse;
  }

  public void setSearchResponse(SearchResponse searchResponse) {
    this.searchResponse = searchResponse;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("SearchEvent{");
    sb.append("searchResponse=").append(searchResponse);
    sb.append('}');
    return sb.toString();
  }
}
