package com.amarinag.talentomobilereview.event;

import com.amarinag.talentomobilereview.domain.entity.weather.WeatherResponse;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   30/5/17
 */
public class WeatherEvent {
  private WeatherResponse weatherResponse;

  public WeatherEvent(WeatherResponse weatherResponse) {
    this.weatherResponse = weatherResponse;
  }

  public WeatherResponse getWeatherResponse() {
    return weatherResponse;
  }

  public void setWeatherResponse(WeatherResponse weatherResponse) {
    this.weatherResponse = weatherResponse;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("WeatherEvent{");
    sb.append("weatherResponse=").append(weatherResponse);
    sb.append('}');
    return sb.toString();
  }
}
