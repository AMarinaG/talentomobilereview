package com.amarinag.talentomobilereview.event;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public class HistoryEvent {
  private List<GeonameEntity> entities;

  public HistoryEvent(List<GeonameEntity> entities) {
    this.entities = entities;
  }

  public List<GeonameEntity> getEntities() {
    return entities;
  }

  public void setEntities(List<GeonameEntity> entities) {
    this.entities = entities;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("HistoryEvent{");
    sb.append("entities=").append(entities);
    sb.append('}');
    return sb.toString();
  }
}
