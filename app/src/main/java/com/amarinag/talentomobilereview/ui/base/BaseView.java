package com.amarinag.talentomobilereview.ui.base;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
public interface BaseView {
  void showError(String message);

  void showError(int message);
}
