package com.amarinag.talentomobilereview.ui.search;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public interface OnClickHistoryListener {
  void onClickHistory(GeonameEntity entity);
}
