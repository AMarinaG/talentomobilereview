package com.amarinag.talentomobilereview.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import butterknife.ButterKnife;
import com.amarinag.talentomobilereview.App;
import com.squareup.leakcanary.RefWatcher;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   24/5/17
 */
public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements BaseView {

  @CallSuper
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutResource());
    injectModule();
    ButterKnife.bind(this);
  }

  protected abstract void injectModule();

  protected abstract int getLayoutResource();

  public abstract P getPresenter();

  @CallSuper
  @Override
  protected void onResume() {
    super.onResume();
    getPresenter().takeView(this);
  }

  @CallSuper
  @Override
  protected void onPause() {
    super.onPause();
    getPresenter().dropView();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    RefWatcher refWatcher = App.get(this).getRefWatcher();
    refWatcher.watch(this);
  }

  @Override
  public void showError(String message) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showError(@StringRes int stringRes) {
    showError(getString(stringRes));
  }
}
