package com.amarinag.talentomobilereview.ui.detail;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.domain.entity.weather.WeatherObservationEntity;
import com.amarinag.talentomobilereview.ui.base.BaseView;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public interface DetailView extends BaseView {
  void addMarker(GeonameEntity title, double latitude, double longitude);

  void centerMap();

  void clearMap();

  void setTitle(String name);

  void showBottomSheet();

  void hideBottomSheet();

  void hideLoading();

  void showLoading();

  void setTemperature(String temperature);

  void populateObservationData(List<WeatherObservationEntity> weatherObservations);

  void animateTemperature(double temp);
}
