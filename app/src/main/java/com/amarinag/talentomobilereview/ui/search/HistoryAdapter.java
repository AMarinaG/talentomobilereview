package com.amarinag.talentomobilereview.ui.search;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.amarinag.talentomobilereview.R;
import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   31/5/17
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
  private List<GeonameEntity> items;
  private OnClickHistoryListener listener;

  @Inject
  public HistoryAdapter() {
    items = new ArrayList<>();
  }

  @Override
  public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
    return new HistoryViewHolder(view);
  }

  @Override
  public void onBindViewHolder(HistoryViewHolder holder, int position) {
    GeonameEntity geoname = items.get(position);
    holder.tvTitle.setText(geoname.getName());
    TextDrawable drawable = TextDrawable.builder()
        .buildRoundRect(geoname.getName().substring(0, 1), Color.DKGRAY, 10);
    holder.ivHistory.setImageDrawable(drawable);
    holder.itemView.setOnClickListener(view -> {
      if (listener != null) {
        listener.onClickHistory(geoname);
      }
    });
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void add(GeonameEntity geonameEntity) {
    items.add(geonameEntity);
    notifyItemInserted(items.size());
  }

  public void add(List<GeonameEntity> geonameEntities) {
    int total = items.size();
    items.addAll(geonameEntities);
    notifyItemRangeInserted(total, geonameEntities.size());
  }

  public OnClickHistoryListener getListener() {
    return listener;
  }

  public void setListener(OnClickHistoryListener listener) {
    this.listener = listener;
  }

  public class HistoryViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_history_img)
    ImageView ivHistory;
    @BindView(R.id.tv_history_title)
    TextView tvTitle;

    public HistoryViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
