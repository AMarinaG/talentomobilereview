package com.amarinag.talentomobilereview.ui.search;

import com.amarinag.talentomobilereview.domain.interactor.FindAllGeonamesHistoryInteractor;
import com.amarinag.talentomobilereview.event.HistoryEvent;
import com.amarinag.talentomobilereview.ui.base.BasePresenter;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
public class SearchPresenter extends BasePresenter<SearchView> {
  private FindAllGeonamesHistoryInteractor findAllGeonamesHistoryInteractor;

  @Inject
  public SearchPresenter(EventBus eventBus, FindAllGeonamesHistoryInteractor findAllGeonamesHistoryInteractor) {
    super(eventBus);
    this.findAllGeonamesHistoryInteractor = findAllGeonamesHistoryInteractor;
  }

  @Override
  public void takeView(SearchView view) {
    super.takeView(view);
    findAllGeonamesHistoryInteractor.invoke();

  }

  @Subscribe
  public void historyEvent(HistoryEvent historyEvent) {
    if (historyEvent.getEntities() != null && historyEvent.getEntities().size() > 0) {
      getView().setHistory(historyEvent.getEntities());
    }
  }
}
