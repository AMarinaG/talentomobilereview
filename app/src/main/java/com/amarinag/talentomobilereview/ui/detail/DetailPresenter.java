package com.amarinag.talentomobilereview.ui.detail;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.domain.entity.weather.WeatherObservationEntity;
import com.amarinag.talentomobilereview.domain.interactor.SearchInteractor;
import com.amarinag.talentomobilereview.domain.interactor.WeatherInteractor;
import com.amarinag.talentomobilereview.event.ErrorEvent;
import com.amarinag.talentomobilereview.event.SearchEvent;
import com.amarinag.talentomobilereview.event.WeatherEvent;
import com.amarinag.talentomobilereview.ui.base.BasePresenter;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import timber.log.Timber;

import java.util.List;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public class DetailPresenter extends BasePresenter<DetailView> {
  private SearchInteractor searchInteractor;
  private WeatherInteractor weatherInteractor;

  @Inject
  public DetailPresenter(SearchInteractor searchInteractor, EventBus eventBus, WeatherInteractor weatherInteractor) {
    super(eventBus);
    this.searchInteractor = searchInteractor;
    this.weatherInteractor = weatherInteractor;
  }

  public void search(String query) {
    Answers.getInstance().logSearch(new com.crashlytics.android.answers.SearchEvent().putQuery(query));
    searchInteractor.invoke(query);
  }

  @Subscribe
  public void weatherEvent(WeatherEvent event) {
    getView().hideLoading();
    List<WeatherObservationEntity> weatherObservationEntities = event.getWeatherResponse().getWeatherObservations();
    int maxLenght = weatherObservationEntities.toString().length() < 99 ? weatherObservationEntities.toString().length() : 99;
    Answers.getInstance().logCustom(new CustomEvent("weather response").putCustomAttribute("weatherObservationEntities", maxLenght));
    if (weatherObservationEntities == null || weatherObservationEntities.size() <= 0) {
      return;
    }
    String temperature = weatherObservationEntities.get(0).getTemperature();
    getView().setTemperature(temperature);
    try {
      getView().animateTemperature(Double.parseDouble(temperature));
    } catch (NumberFormatException nfe) {
      Timber.d("La temperatura no se puede convertir a un Double");
    }
    getView().populateObservationData(event.getWeatherResponse().getWeatherObservations());
  }

  @Subscribe
  public void searchEvent(SearchEvent event) {
    getView().clearMap();
    List<GeonameEntity> geonames = event.getSearchResponse().getGeonames();
    try {
      for (GeonameEntity geoname : geonames) {
        getView().addMarker(
            geoname,
            Double.parseDouble(geoname.getLat()),
            Double.parseDouble(geoname.getLng())
        );
      }
      getView().centerMap();
    } catch (NumberFormatException nfe) {
      Timber.e(nfe, "No se puede parsear la latitud o la longitud");
    }
  }
  @Subscribe
  public void handleError(ErrorEvent event) {
    getView().hideBottomSheet();
    super.handleError(event);
  }

  public void weatherForGeoname(GeonameEntity geoname) {
    getView().showBottomSheet();
    getView().hideLoading();
    weatherInteractor.invoke(geoname.getBbox());
    getView().setTitle(geoname.getName());
  }
}
