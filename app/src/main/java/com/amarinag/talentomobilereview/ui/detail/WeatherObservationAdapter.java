package com.amarinag.talentomobilereview.ui.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.amarinag.talentomobilereview.R;
import com.amarinag.talentomobilereview.domain.entity.weather.WeatherObservationEntity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   31/5/17
 */
public class WeatherObservationAdapter extends RecyclerView.Adapter<WeatherObservationAdapter.WeatherObservationViewHolder> {
  private List<WeatherObservationEntity> items;

  @Inject
  public WeatherObservationAdapter() {
    items = new ArrayList<>();
  }

  @Override
  public WeatherObservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_observation, parent, false);
    return new WeatherObservationViewHolder(view);
  }

  @Override
  public void onBindViewHolder(WeatherObservationViewHolder holder, int position) {
    WeatherObservationEntity weatherObservation = items.get(position);
    holder.tvStationName.setText(weatherObservation.getStationName());
    holder.tvClouds.setText(weatherObservation.getClouds());
    holder.tvTempareature.setText(holder.itemView.getContext().getString(R.string.label_celsius, weatherObservation.getTemperature()));
    holder.tvDate.setText(weatherObservation.getDatetime());
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void add(WeatherObservationEntity weatherObservation) {
    items.add(weatherObservation);
    notifyItemInserted(items.size());
  }

  public void add(List<WeatherObservationEntity> weatherObservations) {
    int total = items.size();
    items.addAll(weatherObservations);
    notifyItemRangeInserted(total, weatherObservations.size());
  }


  public class WeatherObservationViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_station_name)
    TextView tvStationName;
    @BindView(R.id.tv_clouds)
    TextView tvClouds;
    @BindView(R.id.tv_temperature)
    TextView tvTempareature;
    @BindView(R.id.tv_date)
    TextView tvDate;

    public WeatherObservationViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
