package com.amarinag.talentomobilereview.ui.base;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import com.amarinag.talentomobilereview.R;
import com.amarinag.talentomobilereview.event.ErrorEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
public abstract class BasePresenter<View extends BaseView> {
  @Nullable
  private View view;
  private EventBus eventBus;

  protected BasePresenter(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @Nullable
  public View getView() {
    return view;
  }

  @CallSuper
  public void takeView(View view) {
    this.view = view;
    eventBus.register(this);
  }

  @CallSuper
  public void dropView() {
    this.view = null;
    eventBus.unregister(this);
  }

  @Subscribe
  public void handleError(ErrorEvent event) {
    switch (event.getType()) {
      case SERVER_DOWN:
        getView().showError(R.string.error_server_down);
        break;
      case MESSAGE:
        getView().showError(event.getMessage());
        break;
      case UNKNOW:
      default:
        getView().showError(R.string.error_unknow);
    }
  }
}
