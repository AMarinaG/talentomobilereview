package com.amarinag.talentomobilereview.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import com.amarinag.talentomobilereview.App;
import com.amarinag.talentomobilereview.R;
import com.amarinag.talentomobilereview.di.component.DaggerSearchComponent;
import com.amarinag.talentomobilereview.di.component.SearchComponent;
import com.amarinag.talentomobilereview.di.module.SearchModule;
import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.ui.base.BaseActivity;
import com.amarinag.talentomobilereview.ui.detail.DetailActivity;

import java.util.List;

import javax.inject.Inject;

public class SearchActivity extends BaseActivity<SearchPresenter> implements SearchView {
  @Inject
  SearchPresenter presenter;
  @Inject
  HistoryAdapter historyAdapter;
  @BindView(R.id.search_view)
  android.support.v7.widget.SearchView searchView;
  @BindView(R.id.rv_history)
  RecyclerView historyList;
  private SearchComponent component;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        openDetailActivity(query);
        return true;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        return false;
      }
    });
    setupRecycler();
  }

  private void setupRecycler() {
    historyList.setLayoutManager(new GridLayoutManager(this, 4));
    historyList.setAdapter(historyAdapter);
    historyAdapter.setListener(entity -> openDetailActivity(entity.getName()));
  }

  private void openDetailActivity(String query) {
    DetailActivity.start(this, query);
  }

  @Override
  protected void injectModule() {
    component = DaggerSearchComponent.builder()
        .appComponent(App.get(this).getComponent())
        .searchModule(new SearchModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected int getLayoutResource() {
    return R.layout.activity_search;
  }

  @Override
  public SearchPresenter getPresenter() {
    return presenter;
  }

  @Override
  public void setHistory(List<GeonameEntity> entities) {
    historyAdapter.add(entities);
  }
}
