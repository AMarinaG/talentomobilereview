package com.amarinag.talentomobilereview.ui.search;

import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.ui.base.BaseView;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
public interface SearchView extends BaseView {
  void setHistory(List<GeonameEntity> entities);
}
