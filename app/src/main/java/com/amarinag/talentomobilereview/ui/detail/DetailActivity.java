package com.amarinag.talentomobilereview.ui.detail;

import static com.google.android.gms.actions.SearchIntents.EXTRA_QUERY;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import com.amarinag.talentomobilereview.App;
import com.amarinag.talentomobilereview.R;
import com.amarinag.talentomobilereview.di.component.DaggerDetailComponent;
import com.amarinag.talentomobilereview.di.component.DetailComponent;
import com.amarinag.talentomobilereview.di.module.DetailModule;
import com.amarinag.talentomobilereview.domain.entity.search.GeonameEntity;
import com.amarinag.talentomobilereview.domain.entity.weather.WeatherObservationEntity;
import com.amarinag.talentomobilereview.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

public class DetailActivity extends BaseActivity<DetailPresenter> implements OnMapReadyCallback, DetailView {

  @Inject
  DetailPresenter presenter;
  @Inject
  WeatherObservationAdapter weatherObservationAdapter;
  @BindView(R.id.detail_search_view)
  SearchView searchView;
  @BindView(R.id.design_bottom_sheet)
  ConstraintLayout bottomSheetLayout;
  @BindView(R.id.bs_title)
  TextView tvTitle;
  @BindView(R.id.bs_loading)
  ContentLoadingProgressBar pbLoading;
  @BindView(R.id.bs_temp)
  TextView tvTemperature;
  @BindView(R.id.bs_temp_progress)
  ProgressBar tempProgress;
  @BindView(R.id.bs_observations)
  RecyclerView rvObservations;
  private String query;
  private GoogleMap mMap;
  private DetailComponent component;
  private LatLngBounds.Builder latLngBoundsBuilder;
  private BottomSheetBehavior bottomSheetBehavior;

  public static void start(Context context, String query) {
    Intent starter = new Intent(context, DetailActivity.class);
    starter.putExtra(EXTRA_QUERY, query);
    context.startActivity(starter);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle extras = getIntent().getExtras();
    Uri deepLinkUri = getIntent().getData();
    if (extras != null || deepLinkUri != null) {
      String query = null;
      if (extras != null) {
        query = extras.getString(EXTRA_QUERY);
      } else {
        query = getIntent().getData().getLastPathSegment();
      }
      if (query != null && !query.isEmpty()) {
        this.query = query;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setupBottomSheet();
        setupRecycler();
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
          @Override
          public boolean onQueryTextSubmit(String query) {
            getPresenter().search(query);
            return true;
          }

          @Override
          public boolean onQueryTextChange(String newText) {
            return false;
          }
        });
      } else {
        queryNotFound();
      }
    } else {
      queryNotFound();
    }
  }

  private void setupRecycler() {
    rvObservations.setLayoutManager(new LinearLayoutManager(this));
    rvObservations.setAdapter(weatherObservationAdapter);
  }

  private void setupBottomSheet() {
    bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
  }

  @Override
  protected void injectModule() {
    component = DaggerDetailComponent.builder()
        .appComponent(App.get(this).getComponent())
        .detailModule(new DetailModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected int getLayoutResource() {
    return R.layout.activity_detail;
  }

  @Override
  public DetailPresenter getPresenter() {
    return presenter;
  }

  private void queryNotFound() {
    finish();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    mMap.setOnMarkerClickListener(marker -> {
      mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
      getPresenter().weatherForGeoname((GeonameEntity) marker.getTag());
      return true;
    });
    getPresenter().search(query);
  }

  @Override
  public void addMarker(GeonameEntity geoname, double latitude, double longitude) {
    LatLng positionMarker = new LatLng(latitude, longitude);
    latLngBoundsBuilder.include(positionMarker);
    mMap.addMarker(new MarkerOptions().position(positionMarker).title(geoname.getName())).setTag(geoname);
  }

  @Override
  public void centerMap() {
    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBoundsBuilder.build(), 50));
  }

  @Override
  public void clearMap() {
    mMap.clear();
    latLngBoundsBuilder = new LatLngBounds.Builder();
  }

  @Override
  public void setTitle(String name) {
    tvTitle.setText(name);
  }

  @Override
  public void showBottomSheet() {
    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
  }

  @Override
  public void hideBottomSheet() {
    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
  }

  @Override
  public void hideLoading() {
    tvTemperature.setVisibility(View.VISIBLE);
    pbLoading.hide();
  }

  @Override
  public void showLoading() {
    tvTemperature.setVisibility(View.GONE);
    pbLoading.show();
  }

  @Override
  public void setTemperature(String temperature) {
    tvTemperature.setText(getString(R.string.label_celsius, temperature));
  }

  @Override
  public void populateObservationData(List<WeatherObservationEntity> weatherObservations) {
    weatherObservationAdapter.add(weatherObservations);
  }

  @Override
  public void animateTemperature(double temp) {
    CountDownTimer countDownTimer = new CountDownTimer(2000, 100) {
      @Override
      public void onTick(long l) {
        if (l == 2000) {
          tempProgress.setProgress(0);
        } else {
          Double progress = (temp / 20) * (20 - (l / 100)) * 10;
          tempProgress.setProgress(progress.intValue());
          tempProgress.getProgressDrawable().setColorFilter(
              getColorByTemp(progress), android.graphics.PorterDuff.Mode.SRC_IN);
        }
      }

      @Override
      public void onFinish() {
        Double progress = temp * 10;
        tempProgress.setProgress(progress.intValue());
      }
    };
    countDownTimer.start();

  }

  private int getColorByTemp(Double progress) {
    int colorRes = progress < 120 ? R.color.blue_90 :
        progress < 150 ? R.color.blue_80 :
            progress < 180 ? R.color.blue_70 :
                progress < 210 ? R.color.red_70 :
                    progress < 240 ? R.color.red_80 :
                        R.color.red_90;
    return ContextCompat.getColor(DetailActivity.this, colorRes);
  }
}
