package com.amarinag.talentomobilereview.domain.interactor;

import com.amarinag.talentomobilereview.domain.repository.search.SearchRepository;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public class SearchInteractor {
  private SearchRepository searchRepository;

  @Inject
  public SearchInteractor(SearchRepository searchRepository) {
    this.searchRepository = searchRepository;
  }

  public void invoke(String query) {
    searchRepository.cityByQuery(query);
  }
}
