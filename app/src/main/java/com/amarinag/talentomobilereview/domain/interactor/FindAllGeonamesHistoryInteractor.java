package com.amarinag.talentomobilereview.domain.interactor;

import com.amarinag.talentomobilereview.domain.repository.search.SearchRepository;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   1/6/17
 */
public class FindAllGeonamesHistoryInteractor {
  private SearchRepository searchRepository;

  @Inject
  public FindAllGeonamesHistoryInteractor(SearchRepository searchRepository) {
    this.searchRepository = searchRepository;
  }

  public void invoke() {
    searchRepository.findAllGeonamesHistory();
  }
}
