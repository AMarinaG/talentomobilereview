package com.amarinag.talentomobilereview.domain.service;

import com.amarinag.talentomobilereview.domain.entity.weather.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public interface WeatherService {
  @GET("weatherJSON")
  Call<WeatherResponse> weatherByCardinals(
      @Query("north") float north,
      @Query("south") float south,
      @Query("east") float east,
      @Query("west") float west,
      @Query("username") String username

  );
}
