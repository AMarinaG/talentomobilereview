package com.amarinag.talentomobilereview.domain.interactor;

import com.amarinag.talentomobilereview.domain.entity.search.BboxEntity;
import com.amarinag.talentomobilereview.domain.repository.weather.WeatherRepository;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   30/5/17
 */
public class WeatherInteractor {
  private WeatherRepository repository;

  @Inject
  public WeatherInteractor(WeatherRepository repository) {
    this.repository = repository;
  }

  public void invoke(BboxEntity bbox) {
    repository.searchWeatherBy4Points(bbox);
  }
}
