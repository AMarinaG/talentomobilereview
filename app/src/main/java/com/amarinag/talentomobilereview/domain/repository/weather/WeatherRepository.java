package com.amarinag.talentomobilereview.domain.repository.weather;

import com.amarinag.talentomobilereview.BuildConfig;
import com.amarinag.talentomobilereview.domain.entity.search.BboxEntity;
import com.amarinag.talentomobilereview.domain.entity.weather.WeatherResponse;
import com.amarinag.talentomobilereview.domain.service.WeatherService;
import com.amarinag.talentomobilereview.event.ErrorEvent;
import com.amarinag.talentomobilereview.event.WeatherEvent;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public class WeatherRepository {
  private WeatherService service;

  @Inject
  public WeatherRepository(Retrofit retrofit) {
    this.service = retrofit.create(WeatherService.class);
  }

  public void searchWeatherBy4Points(BboxEntity bbox) {
    if (bbox == null) {
      EventBus.getDefault().post(ErrorEvent.createErrorEvent("Datos incorrectos"));
      return;
    }
    Call<WeatherResponse> callWeather = service.weatherByCardinals(
        bbox.getNorth(),
        bbox.getSouth(),
        bbox.getEast(),
        bbox.getWest(),
        BuildConfig.USERNAME_TEST
    );

    callWeather.enqueue(new Callback<WeatherResponse>() {
      @Override
      public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
        Timber.d("Response: %s", response);
        if (response.isSuccessful()) {
          Timber.d("OK!");
          EventBus.getDefault().post(new WeatherEvent(response.body()));
        } else {
          Timber.d("KO!");
        }
      }

      @Override
      public void onFailure(Call<WeatherResponse> call, Throwable t) {
        EventBus.getDefault().post(ErrorEvent.createServerDownError());
      }
    });

  }
}

