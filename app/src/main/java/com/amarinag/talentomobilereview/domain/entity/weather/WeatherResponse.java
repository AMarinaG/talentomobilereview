package com.amarinag.talentomobilereview.domain.entity.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   30/5/17
 */
public class WeatherResponse {
  @SerializedName("weatherObservations")
  @Expose
  private List<WeatherObservationEntity> weatherObservations = null;

  public List<WeatherObservationEntity> getWeatherObservations() {
    return weatherObservations;
  }

  public void setWeatherObservations(List<WeatherObservationEntity> weatherObservations) {
    this.weatherObservations = weatherObservations;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("WeatherResponse{");
    sb.append("weatherObservations=").append(weatherObservations);
    sb.append('}');
    return sb.toString();
  }
}
