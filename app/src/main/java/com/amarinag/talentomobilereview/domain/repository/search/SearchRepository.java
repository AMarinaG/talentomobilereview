package com.amarinag.talentomobilereview.domain.repository.search;

import com.amarinag.talentomobilereview.BuildConfig;
import com.amarinag.talentomobilereview.db.DatabaseService;
import com.amarinag.talentomobilereview.domain.entity.search.SearchResponse;
import com.amarinag.talentomobilereview.domain.service.SearchService;
import com.amarinag.talentomobilereview.event.ErrorEvent;
import com.amarinag.talentomobilereview.event.HistoryEvent;
import com.amarinag.talentomobilereview.event.SearchEvent;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public class SearchRepository {
  private final SearchService searchService;
  private final DatabaseService databaseService;

  @Inject
  public SearchRepository(Retrofit retrofit, DatabaseService databaseService) {
    this.searchService = retrofit.create(SearchService.class);
    this.databaseService = databaseService;
  }

  public void cityByQuery(String query) {
    Call<SearchResponse> searchResponseCall = searchService.searchCity(
        query,
        BuildConfig.DEFAULT_MAX_ROWS,
        BuildConfig.DEFAULT_START_ROWS,
        BuildConfig.DEFAULT_LANGUAGE,
        BuildConfig.DEFAULT_IS_NAME_REQUIRED,
        BuildConfig.DEFAULT_STYLE,
        BuildConfig.USERNAME_TEST
    );
    searchResponseCall.enqueue(new Callback<SearchResponse>() {
      @Override
      public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
        if (response.isSuccessful()) {
          databaseService.saveGeoname(response.body().getGeonames());
          EventBus.getDefault().post(new SearchEvent(response.body()));
        }
      }

      @Override
      public void onFailure(Call<SearchResponse> call, Throwable t) {
        Timber.e(t, "Error %s", t.getMessage());
        EventBus.getDefault().post(ErrorEvent.createServerDownError());
      }
    });
  }

  public void findAllGeonamesHistory() {
    EventBus.getDefault().post(new HistoryEvent(databaseService.getGeonames()));
  }
}
