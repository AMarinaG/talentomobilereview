package com.amarinag.talentomobilereview.domain.entity.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   29/5/17
 */
public class AlternateNameEntity {
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("lang")
  @Expose
  private String lang;
  @SerializedName("isShortName")
  @Expose
  private boolean isShortName;
  @SerializedName("isPreferredName")
  @Expose
  private boolean isPreferredName;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public boolean isShortName() {
    return isShortName;
  }

  public void setShortName(boolean shortName) {
    isShortName = shortName;
  }

  public boolean isPreferredName() {
    return isPreferredName;
  }

  public void setPreferredName(boolean preferredName) {
    isPreferredName = preferredName;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("AlternateNameEntity{");
    sb.append("name='").append(name).append('\'');
    sb.append(", lang='").append(lang).append('\'');
    sb.append(", isShortName=").append(isShortName);
    sb.append(", isPreferredName=").append(isPreferredName);
    sb.append('}');
    return sb.toString();
  }
}
