package com.amarinag.talentomobilereview.domain.service;

import com.amarinag.talentomobilereview.domain.entity.search.SearchResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public interface SearchService {
  @GET("searchJSON")
  Call<SearchResponse> searchCity(
      @Query("q") String query,
      @Query("maxRows") int maxRows,
      @Query("startRows") int startRows,
      @Query("lang") String language,
      @Query("required") boolean required,
      @Query("style") String style,
      @Query("username") String username

  );
}