package com.amarinag.talentomobilereview.domain.entity.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   30/5/17
 */
public class WeatherObservationEntity {
  @SerializedName("lng")
  @Expose
  private float lng;
  @SerializedName("observation")
  @Expose
  private String observation;
  @SerializedName("ICAO")
  @Expose
  private String iCAO;
  @SerializedName("clouds")
  @Expose
  private String clouds;
  @SerializedName("dewPoint")
  @Expose
  private String dewPoint;
  @SerializedName("cloudsCode")
  @Expose
  private String cloudsCode;
  @SerializedName("datetime")
  @Expose
  private String datetime;
  @SerializedName("seaLevelPressure")
  @Expose
  private float seaLevelPressure;
  @SerializedName("temperature")
  @Expose
  private String temperature;
  @SerializedName("humidity")
  @Expose
  private int humidity;
  @SerializedName("stationName")
  @Expose
  private String stationName;
  @SerializedName("weatherCondition")
  @Expose
  private String weatherCondition;
  @SerializedName("windDirection")
  @Expose
  private int windDirection;
  @SerializedName("windSpeed")
  @Expose
  private String windSpeed;
  @SerializedName("lat")
  @Expose
  private float lat;
  @SerializedName("weatherConditionCode")
  @Expose
  private String weatherConditionCode;

  public float getLng() {
    return lng;
  }

  public void setLng(float lng) {
    this.lng = lng;
  }

  public String getObservation() {
    return observation;
  }

  public void setObservation(String observation) {
    this.observation = observation;
  }

  public String getiCAO() {
    return iCAO;
  }

  public void setiCAO(String iCAO) {
    this.iCAO = iCAO;
  }

  public String getClouds() {
    return clouds;
  }

  public void setClouds(String clouds) {
    this.clouds = clouds;
  }

  public String getDewPoint() {
    return dewPoint;
  }

  public void setDewPoint(String dewPoint) {
    this.dewPoint = dewPoint;
  }

  public String getCloudsCode() {
    return cloudsCode;
  }

  public void setCloudsCode(String cloudsCode) {
    this.cloudsCode = cloudsCode;
  }

  public String getDatetime() {
    return datetime;
  }

  public void setDatetime(String datetime) {
    this.datetime = datetime;
  }

  public float getSeaLevelPressure() {
    return seaLevelPressure;
  }

  public void setSeaLevelPressure(float seaLevelPressure) {
    this.seaLevelPressure = seaLevelPressure;
  }

  public String getTemperature() {
    return temperature;
  }

  public void setTemperature(String temperature) {
    this.temperature = temperature;
  }

  public int getHumidity() {
    return humidity;
  }

  public void setHumidity(int humidity) {
    this.humidity = humidity;
  }

  public String getStationName() {
    return stationName;
  }

  public void setStationName(String stationName) {
    this.stationName = stationName;
  }

  public String getWeatherCondition() {
    return weatherCondition;
  }

  public void setWeatherCondition(String weatherCondition) {
    this.weatherCondition = weatherCondition;
  }

  public int getWindDirection() {
    return windDirection;
  }

  public void setWindDirection(int windDirection) {
    this.windDirection = windDirection;
  }

  public String getWindSpeed() {
    return windSpeed;
  }

  public void setWindSpeed(String windSpeed) {
    this.windSpeed = windSpeed;
  }

  public float getLat() {
    return lat;
  }

  public void setLat(float lat) {
    this.lat = lat;
  }

  public String getWeatherConditionCode() {
    return weatherConditionCode;
  }

  public void setWeatherConditionCode(String weatherConditionCode) {
    this.weatherConditionCode = weatherConditionCode;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("WeatherObservationEntity{");
    sb.append("lng=").append(lng);
    sb.append(", observation='").append(observation).append('\'');
    sb.append(", iCAO='").append(iCAO).append('\'');
    sb.append(", clouds='").append(clouds).append('\'');
    sb.append(", dewPoint='").append(dewPoint).append('\'');
    sb.append(", cloudsCode='").append(cloudsCode).append('\'');
    sb.append(", datetime='").append(datetime).append('\'');
    sb.append(", seaLevelPressure=").append(seaLevelPressure);
    sb.append(", temperature='").append(temperature).append('\'');
    sb.append(", humidity=").append(humidity);
    sb.append(", stationName='").append(stationName).append('\'');
    sb.append(", weatherCondition='").append(weatherCondition).append('\'');
    sb.append(", windDirection=").append(windDirection);
    sb.append(", windSpeed='").append(windSpeed).append('\'');
    sb.append(", lat=").append(lat);
    sb.append(", weatherConditionCode='").append(weatherConditionCode).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
