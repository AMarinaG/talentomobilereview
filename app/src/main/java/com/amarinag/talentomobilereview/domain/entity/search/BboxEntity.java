package com.amarinag.talentomobilereview.domain.entity.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   29/5/17
 */
public class BboxEntity {
  @SerializedName("east")
  @Expose
  private float east;
  @SerializedName("south")
  @Expose
  private float south;
  @SerializedName("north")
  @Expose
  private float north;
  @SerializedName("west")
  @Expose
  private float west;
  @SerializedName("accuracyLevel")
  @Expose
  private int accuracyLevel;

  public float getEast() {
    return east;
  }

  public void setEast(float east) {
    this.east = east;
  }

  public float getSouth() {
    return south;
  }

  public void setSouth(float south) {
    this.south = south;
  }

  public float getNorth() {
    return north;
  }

  public void setNorth(float north) {
    this.north = north;
  }

  public float getWest() {
    return west;
  }

  public void setWest(float west) {
    this.west = west;
  }

  public int getAccuracyLevel() {
    return accuracyLevel;
  }

  public void setAccuracyLevel(int accuracyLevel) {
    this.accuracyLevel = accuracyLevel;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("BboxEntity{");
    sb.append("east=").append(east);
    sb.append(", south=").append(south);
    sb.append(", north=").append(north);
    sb.append(", west=").append(west);
    sb.append(", accuracyLevel=").append(accuracyLevel);
    sb.append('}');
    return sb.toString();
  }
}
