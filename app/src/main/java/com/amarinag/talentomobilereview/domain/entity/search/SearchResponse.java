package com.amarinag.talentomobilereview.domain.entity.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
public class SearchResponse {
  @SerializedName("totalResultsCount")
  @Expose
  private int totalResultsCount;
  @SerializedName("geonames")
  @Expose
  private List<GeonameEntity> geonames = null;

  public int getTotalResultsCount() {
    return totalResultsCount;
  }

  public void setTotalResultsCount(int totalResultsCount) {
    this.totalResultsCount = totalResultsCount;
  }

  public List<GeonameEntity> getGeonames() {
    return geonames;
  }

  public void setGeonames(List<GeonameEntity> geonames) {
    this.geonames = geonames;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("SearchResponse{");
    sb.append("totalResultsCount=").append(totalResultsCount);
    sb.append(", geonames=").append(geonames);
    sb.append('}');
    return sb.toString();
  }
}
