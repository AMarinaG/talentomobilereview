package com.amarinag.talentomobilereview.domain.entity.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   29/5/17
 */
public class TimezoneEntity {
  @SerializedName("gmtOffset")
  @Expose
  private double gmtOffset;
  @SerializedName("timeZoneId")
  @Expose
  private String timeZoneId;
  @SerializedName("dstOffset")
  @Expose
  private double dstOffset;

  public double getGmtOffset() {
    return gmtOffset;
  }

  public void setGmtOffset(double gmtOffset) {
    this.gmtOffset = gmtOffset;
  }

  public String getTimeZoneId() {
    return timeZoneId;
  }

  public void setTimeZoneId(String timeZoneId) {
    this.timeZoneId = timeZoneId;
  }

  public double getDstOffset() {
    return dstOffset;
  }

  public void setDstOffset(double dstOffset) {
    this.dstOffset = dstOffset;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("TimezoneEntity{");
    sb.append("gmtOffset=").append(gmtOffset);
    sb.append(", timeZoneId='").append(timeZoneId).append('\'');
    sb.append(", dstOffset=").append(dstOffset);
    sb.append('}');
    return sb.toString();
  }
}
