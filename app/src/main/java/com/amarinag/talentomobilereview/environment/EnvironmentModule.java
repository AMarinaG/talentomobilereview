package com.amarinag.talentomobilereview.environment;

import com.google.gson.Gson;

import android.content.Context;
import android.support.annotation.NonNull;
import com.amarinag.talentomobilereview.App;
import com.amarinag.talentomobilereview.BuildConfig;
import com.amarinag.talentomobilereview.di.ForApplication;
import com.amarinag.talentomobilereview.network.OkHttpInterceptors;
import com.amarinag.talentomobilereview.network.OkHttpNetworkInterceptors;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@Module
public class EnvironmentModule {
  private static final int DISK_CACHE_SIZE = (int) 1_000_000L;
  private final App app;

  public EnvironmentModule(App app) {
    this.app = app;
  }

  @Provides
  @Singleton
  public OkHttpClient provideOkHttpClient(
      @ForApplication Context app,
      @OkHttpInterceptors @NonNull List<Interceptor> interceptors,
      @OkHttpNetworkInterceptors @NonNull List<Interceptor> networkInterceptors) {
    File cacheDir = new File(app.getCacheDir(), "http");
    Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

    final OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
    for (Interceptor interceptor : interceptors) {
      okHttpBuilder.addInterceptor(interceptor);
    }
    for (Interceptor networkInterceptor : networkInterceptors) {
      okHttpBuilder.addNetworkInterceptor(networkInterceptor);
    }

    okHttpBuilder.cache(cache);
    okHttpBuilder.readTimeout(30, TimeUnit.SECONDS);
    okHttpBuilder.writeTimeout(30, TimeUnit.SECONDS);
    okHttpBuilder.connectTimeout(30, TimeUnit.SECONDS);
    return okHttpBuilder.build();
  }

  @Provides
  @Singleton
  public Retrofit provideRetrofit(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(BuildConfig.API_ENDPOINT)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    return retrofit;
  }
}
