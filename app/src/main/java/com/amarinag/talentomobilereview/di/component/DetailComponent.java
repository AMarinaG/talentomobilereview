package com.amarinag.talentomobilereview.di.component;

import com.amarinag.talentomobilereview.di.ActivityScope;
import com.amarinag.talentomobilereview.di.module.DetailModule;
import com.amarinag.talentomobilereview.ui.detail.DetailActivity;
import dagger.Component;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = DetailModule.class)
public interface DetailComponent {
  void inject(DetailActivity activity);
}
