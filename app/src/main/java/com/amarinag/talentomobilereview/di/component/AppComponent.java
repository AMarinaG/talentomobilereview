package com.amarinag.talentomobilereview.di.component;

import com.google.gson.Gson;

import android.content.Context;
import com.amarinag.talentomobilereview.App;
import com.amarinag.talentomobilereview.db.DatabaseService;
import com.amarinag.talentomobilereview.di.ForApplication;
import com.amarinag.talentomobilereview.di.module.AndroidModule;
import com.amarinag.talentomobilereview.di.module.AppModule;
import com.amarinag.talentomobilereview.environment.EnvironmentModule;
import com.amarinag.talentomobilereview.network.OkHttpInterceptorModule;
import com.amarinag.talentomobilereview.util.gson.GsonModule;
import dagger.Component;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Retrofit;

import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@Singleton
@Component(modules = {
    AppModule.class,
    AndroidModule.class,
    GsonModule.class,
    EnvironmentModule.class,
    OkHttpInterceptorModule.class
})
public interface AppComponent {
  @ForApplication
  Context provideContext();

  Retrofit provideRetrofit();

  Gson provideGson();

  EventBus provideEventBus();

  DatabaseService provideDatabaseService();

  void inject(App app);

  final class Initializer {
    public static AppComponent init(App app) {
      return DaggerAppComponent.builder()
          .appModule(new AppModule(app))
          .androidModule(new AndroidModule())
          .gsonModule(new GsonModule())
          .environmentModule(new EnvironmentModule(app))
          .okHttpInterceptorModule(new OkHttpInterceptorModule())
          .build();
    }
  }
}
