package com.amarinag.talentomobilereview.di.component;

import com.amarinag.talentomobilereview.di.ActivityScope;
import com.amarinag.talentomobilereview.di.module.SearchModule;
import com.amarinag.talentomobilereview.ui.search.SearchActivity;
import dagger.Component;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = SearchModule.class)
public interface SearchComponent {
  void inject(SearchActivity activity);
}
