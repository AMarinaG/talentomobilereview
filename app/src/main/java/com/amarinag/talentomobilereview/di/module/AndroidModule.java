package com.amarinag.talentomobilereview.di.module;

import android.app.ActivityManager;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;
import com.amarinag.talentomobilereview.di.ForApplication;
import dagger.Module;
import dagger.Provides;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@Module
public class AndroidModule {
  @Provides
  public SharedPreferences provideSharedPreferences(@ForApplication Context application) {
    return application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
  }

  @Provides
  public LayoutInflater provideLayoutInflater(@ForApplication Application application) {
    return LayoutInflater.from(application);
  }

  @Provides
  public InputMethodManager provideInputMethodManager(@ForApplication Application application) {
    return (InputMethodManager) application.getSystemService(Context.INPUT_METHOD_SERVICE);
  }

  @Provides
  public NotificationManager provideNotificationManager(@ForApplication Context context) {
    return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
  }

  @Provides
  public ActivityManager provideActivityManager(@ForApplication Application application) {
    return (ActivityManager) application.getSystemService(Context.ACTIVITY_SERVICE);
  }
}
