package com.amarinag.talentomobilereview.di.module;

import com.amarinag.talentomobilereview.di.ActivityScope;
import com.amarinag.talentomobilereview.ui.base.BaseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@ActivityScope
@Module
public class ActivityModule {
  private BaseActivity activity;

  public ActivityModule(BaseActivity activity) {
    this.activity = activity;
  }

  @Provides
  protected BaseActivity activity() {
    return activity;
  }
}
