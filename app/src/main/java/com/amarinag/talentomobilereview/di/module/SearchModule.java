package com.amarinag.talentomobilereview.di.module;

import com.amarinag.talentomobilereview.ui.base.BaseActivity;
import dagger.Module;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@Module
public class SearchModule extends ActivityModule {
  public SearchModule(BaseActivity activity) {
    super(activity);
  }
}
