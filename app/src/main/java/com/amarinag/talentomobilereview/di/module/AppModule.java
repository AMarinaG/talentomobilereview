package com.amarinag.talentomobilereview.di.module;

import android.content.Context;
import com.amarinag.talentomobilereview.App;
import com.amarinag.talentomobilereview.db.DatabaseHelper;
import com.amarinag.talentomobilereview.db.DatabaseService;
import com.amarinag.talentomobilereview.db.DatabaseServiceImpl;
import com.amarinag.talentomobilereview.di.ForApplication;
import dagger.Module;
import dagger.Provides;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   27/5/17
 */
@Module
public class AppModule {
  private App app;

  public AppModule(App app) {
    this.app = app;
  }

  @ForApplication
  @Provides
  @Singleton
  public App provideApp() {
    return app;
  }

  @ForApplication
  @Provides
  @Singleton
  public Context provideContext() {
    return app.getApplicationContext();
  }

  @Provides
  @Singleton
  public EventBus provideEventBus() {
    return EventBus.getDefault();
  }

  @Provides
  @Singleton
  public DatabaseService provideDatabaseService(DatabaseHelper databaseHelper) {
    return new DatabaseServiceImpl(databaseHelper.getGeonameDao());
  }
}
