package com.amarinag.talentomobilereview.di.module;

import com.amarinag.talentomobilereview.ui.base.BaseActivity;
import dagger.Module;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@Module
public class DetailModule extends ActivityModule {
  public DetailModule(BaseActivity activity) {
    super(activity);
  }
}
