package com.amarinag.talentomobilereview.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * TalentoMobileReview.
 *
 * @author -   AMarinaG
 * @since -   28/5/17
 */
@Module
public class GsonModule {
  @Provides
  @Singleton
  public GsonBuilder provideGsonBuilder() {
    GsonBuilder gsonBuilder = new GsonBuilder();

    gsonBuilder.excludeFieldsWithoutExposeAnnotation();
    return gsonBuilder;
  }

  @Provides
  @Singleton
  public Gson provideGson(GsonBuilder gsonBuilder) {
    return gsonBuilder.create();
  }
}
